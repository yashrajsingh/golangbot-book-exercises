package main

import (
	"fmt"
	"time"
)

func server1(ch chan string) {
	time.Sleep(6 * time.Second)
	ch <- "from server1"
}

func server2(ch chan string) {
	time.Sleep(3 * time.Second)
	ch <- "from server2"
}

func process(ch chan string) {
	time.Sleep(5000 * time.Millisecond)
	ch <- "process successful"
}

func main() {
	output1 := make(chan string)
	output2 := make(chan string)
	go server1(output1)
	go server2(output2)
	select { // select statement blocks until one case is ready
	case s1 := <-output1:
		fmt.Println(s1)
	case s2 := <-output2: // random case will be selected if both return at same time
		fmt.Println(s2)
	}

	fmt.Println()

	ch := make(chan string)
	select {
	case <-ch:
	default:
		fmt.Println("default case executed")
	}

	fmt.Println()

	ch1 := make(chan string)
	go process(ch1)
	for {
		time.Sleep(1000 * time.Millisecond)
		select {
		case v := <-ch1:
			fmt.Println("received value: ", v)
			return
		default: // used for running non-blocking code
			fmt.Println("no value received")
		}
	}
}
