package main

import (
	"fmt"
	"sync"
)

var x = 0

func increment(wg *sync.WaitGroup) {
	x = x + 1 // will create race condition
	wg.Done()
}

func incrementMutex(wg *sync.WaitGroup, m *sync.Mutex) {
	m.Lock()
	x = x + 1
	m.Unlock()
	wg.Done()
}

func incrementChannel(wg *sync.WaitGroup, ch chan bool) {
	ch <- true
	x = x + 1
	<-ch
	wg.Done()
}

func main() {
	fmt.Println("Race condition call:")
	var w sync.WaitGroup
	for i := 0; i < 1000; i++ {
		w.Add(1)
		go increment(&w)
	}
	w.Wait()
	fmt.Println("final value of x", x)

	fmt.Println()
	x = 0

	fmt.Println("Race condition fixed using mutex call:")
	var m sync.Mutex
	for i := 0; i < 1000; i++ {
		w.Add(1)
		go incrementMutex(&w, &m)
	}
	w.Wait()
	fmt.Println("final value of x", x)

	fmt.Println()
	x = 0

	fmt.Println("Race condition fixed using channels call:")
	ch := make(chan bool, 1)
	for i := 0; i < 1000; i++ {
		w.Add(1)
		go incrementChannel(&w, ch)
	}
	w.Wait()
	fmt.Println("final value of x", x)
}
