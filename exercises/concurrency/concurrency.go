package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func hello() {
	fmt.Println("Hello world goroutine")
}

func numbers() {
	for i := 1; i <= 5; i++ {
		time.Sleep(250 * time.Millisecond)
		fmt.Printf("%d ", i)
	}
}
func alphabets() {
	for i := 'a'; i <= 'e'; i++ {
		time.Sleep(400 * time.Millisecond)
		fmt.Printf("%c ", i)
	}
}

func hello1(done chan bool) {
	fmt.Println("hello go routine is going to sleep")
	time.Sleep(4 * time.Second)
	fmt.Println("hello go routine awake and going to write to done")
	done <- true
}

func calcSquares(number int, squareop chan int) {
	sum := 0
	for number != 0 {
		digit := number % 10
		sum += digit * digit
		number /= 10
	}
	squareop <- sum
}

func calcCubes(number int, cubeop chan int) {
	sum := 0
	for number != 0 {
		digit := number % 10
		sum += digit * digit * digit
		number /= 10
	}
	cubeop <- sum
}

func sendData(sendch chan<- int) {
	sendch <- 10
}

func producer(chnl chan int) {
	for i := 0; i < 10; i++ {
		chnl <- i
	}
	close(chnl)
}

func write(ch chan int) {
	for i := 0; i < 5; i++ {
		ch <- i
		fmt.Println("successfully wrote", i, "to ch")
	}
	close(ch)
}

func process(i int, wg *sync.WaitGroup) {
	fmt.Println("started Goroutine ", i)
	time.Sleep(2 * time.Second)
	fmt.Printf("Goroutine %d ended\n", i)
	wg.Done()
}

func main() {
	n := runtime.NumCPU()
	fmt.Println("number of CPUs:", n)

	fmt.Println()

	// In concurrency control is always shared, this is not parallilism
	go hello()
	time.Sleep(1 * time.Second)
	fmt.Println("main function")

	fmt.Println()

	go numbers()
	go alphabets()
	time.Sleep(2500 * time.Millisecond)
	fmt.Println("main terminated")

	fmt.Println()

	var a chan int
	if a == nil {
		fmt.Println("channel a is nil, going to define it")
		a = make(chan int)
		fmt.Printf("Type of a is %T", a)
	}

	fmt.Println()

	// Sample channel usage
	done := make(chan bool)
	go hello1(done)
	<-done

	fmt.Println()

	number := 589
	sqrch := make(chan int)
	cubech := make(chan int)
	go calcSquares(number, sqrch)
	go calcCubes(number, cubech)
	squares, cubes := <-sqrch, <-cubech
	fmt.Println("Final output", squares+cubes)

	fmt.Println()

	chnl := make(chan int)
	go sendData(chnl)
	fmt.Println(<-chnl)

	fmt.Println()

	ch := make(chan int)
	go producer(ch)
	// for {
	// 	v, ok := <-ch
	// 	if ok == false {
	// 		break
	// 	}
	// 	fmt.Println("Received ", v, ok)
	// }
	for v := range ch {
		fmt.Println("Received ", v)
	}

	fmt.Println()

	// Buffered Channels
	ch1 := make(chan string, 2)
	ch1 <- "naveen"
	ch1 <- "paul"
	fmt.Println(<-ch1)
	fmt.Println(<-ch1)

	fmt.Println()

	ch2 := make(chan int, 2)
	go write(ch2)
	time.Sleep(2 * time.Second)
	for v := range ch2 {
		fmt.Println("read value", v, "from ch")
		time.Sleep(2 * time.Second)
	}

	fmt.Println()

	// Channel length and capacity
	ch3 := make(chan string, 3)
	ch3 <- "naveen"
	ch3 <- "paul"
	fmt.Println("capacity is", cap(ch3))
	fmt.Println("length is", len(ch3))
	fmt.Println("read value", <-ch3)
	fmt.Println("new length is", len(ch3))

	fmt.Println()

	fmt.Println("Starting goroutines with waitgroup..")
	no := 3
	var wg sync.WaitGroup
	for i := 0; i < no; i++ {
		wg.Add(1)
		go process(i, &wg)
	}
	wg.Wait()
	fmt.Println("All go routines finished executing")
}
