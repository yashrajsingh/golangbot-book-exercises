package main

import (
	"fmt"
)

func main() {
	var personSalary map[string]int
	if personSalary == nil {
		fmt.Println("map is nil. Going to make one.")
		personSalary = make(map[string]int)
	}

	fmt.Println()

	personSalary = make(map[string]int)
	personSalary["steve"] = 12000
	personSalary["jamie"] = 15000
	personSalary["mike"] = 9000
	fmt.Println("personSalary map contents:", personSalary)

	fmt.Println()

	personSalary = map[string]int{
		"steve": 12000,
		"jamie": 15000,
	}
	personSalary["mike"] = 9000
	fmt.Println("personSalary map contents:", personSalary)

	fmt.Println()

	personSalary = map[string]int{
		"steve": 12000,
		"jamie": 15000,
	}
	personSalary["mike"] = 9000
	newEmp := "joe"
	value, ok := personSalary[newEmp]
	if ok == true {
		fmt.Println("Salary of", newEmp, "is", value)
	} else {
		fmt.Println(newEmp, "not found")
	}

	fmt.Println()

	// One important fact is that the order of the retrieval of values from a map when using for range is not guaranteed to be the same for each execution of the program.
	// Because map is a heap internally
	fmt.Println("All items of a map")
	for key, value := range personSalary {
		fmt.Printf("personSalary[%s] = %d\n", key, value)
	}

	fmt.Println()

	fmt.Println("map before deletion", personSalary)
	delete(personSalary, "steve")
	fmt.Println("map after deletion", personSalary)

	fmt.Println("length is", len(personSalary))

	fmt.Println()

	// Maps are reference types, changes made to copy are visible in original value
	personSalary["mike"] = 9000
	fmt.Println("Original person salary", personSalary)
	newPersonSalary := personSalary
	newPersonSalary["mike"] = 18000
	fmt.Println("Person salary changed", personSalary)

	fmt.Println()

	// myvar := make(map[string]string)
	myvar := map[string]string{}
	myvar["what"] = "shit"
	fmt.Println(myvar)
}
