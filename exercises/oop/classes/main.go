package main

import (
	"fmt"

	"gitlab.com/yashrajsingh/golangbot-book-exercises/exercises/oop/classes/employee"
)

func main() {
	// Zero value
	var e employee.Employee
	e.LeavesRemaining()

	fmt.Println()

	e1 := employee.Employee{
		FirstName:   "Sam",
		LastName:    "Adolf",
		TotalLeaves: 30,
		LeavesTaken: 20,
	}
	e1.LeavesRemaining()

	fmt.Println()

	e2 := employee.New("Sam", "Adolf", 30, 20)
	e2.LeavesRemaining()

	fmt.Println()
}
