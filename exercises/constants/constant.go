package main

import (
	"fmt"
	"math"
	"reflect"
	"unsafe"
)

const hello = "Hello World"

func main() {
	var a = math.Sqrt(4) //allowed
	fmt.Println("a:", a)

	fmt.Println()

	fmt.Println("type of a:", reflect.TypeOf(hello))
	fmt.Printf("\ntype: %T, size: %d, value: %s\n\n", hello, unsafe.Sizeof(hello), hello)

	fmt.Println()

	const b = 5
	fmt.Println("type of b:", reflect.TypeOf(b))
	var intVar int = b
	var int32Var int32 = b
	var float64Var float64 = b
	var complex64Var complex64 = b
	fmt.Println("intVar", intVar, "\nint32Var", int32Var, "\nfloat64Var", float64Var, "\ncomplex64Var", complex64Var)

	fmt.Println()

	var c = 5.9 / 8
	fmt.Printf("\nc's type %T value %v\n", c, c)
}
