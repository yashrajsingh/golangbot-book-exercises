package main

import (
	"fmt"
	"math"
)

type Employee struct {
	name     string
	salary   int
	currency string
	age      int
}

type Rectangle struct {
	length int
	width  int
}

type Circle struct {
	radius float64
}

func (r Rectangle) Area() int {
	return r.length * r.width
}

func (c Circle) Area() float64 {
	return math.Pi * c.radius * c.radius
}

type address struct {
	city  string
	state string
}

func (a address) fullAddress() {
	fmt.Printf("Full address: %s, %s", a.city, a.state)
}

type person struct {
	firstName string
	lastName  string
	address
}

/* displaySalary() method has Employee as the receiver type */
func (e Employee) displaySalary() {
	fmt.Printf("Salary of %s is %s%d", e.name, e.currency, e.salary)
}

/* Method with pointer receiver */
func (e *Employee) changeAge(newAge int) {
	e.age = newAge
}

type rectangle struct {
	length int
	width  int
}

func area(r rectangle) {
	fmt.Printf("Area Function result: %d\n", (r.length * r.width))
}

func (r rectangle) area() {
	fmt.Printf("Area Method result: %d\n", (r.length * r.width))
}

type myInt int // type alias

func (a myInt) add(b myInt) myInt {
	return a + b
}

func main() {
	emp1 := Employee{
		name:     "Sam Adolf",
		salary:   5000,
		currency: "$",
		age:      24,
	}
	emp1.displaySalary() //Calling displaySalary() method of Employee type

	fmt.Println()

	r := Rectangle{
		length: 10,
		width:  5,
	}
	fmt.Printf("Area of rectangle %d\n", r.Area())
	c := Circle{
		radius: 12,
	}
	fmt.Printf("Area of circle %f", c.Area())

	fmt.Println()

	fmt.Printf("\n\nEmployee age before change: %d", emp1.age)
	// (&emp1).changeAge(51)
	emp1.changeAge(51)
	fmt.Printf("\nEmployee age after change: %d", emp1.age)

	fmt.Println()

	p := person{
		firstName: "Elon",
		lastName:  "Musk",
		address: address{
			city:  "Los Angeles",
			state: "California",
		},
	}
	p.fullAddress() //accessing fullAddress method of address struct

	fmt.Println()

	r1 := rectangle{
		length: 10,
		width:  5,
	}
	area(r1)
	r1.area()

	fmt.Println()

	p1 := &r1
	/*
	   compilation error, cannot use p (type *rectangle) as type rectangle
	   in argument to area
	*/
	// area(p)

	p1.area() //calling value receiver with a pointer

	fmt.Println()

	num1 := myInt(5)
	num2 := myInt(10)
	sum := num1.add(num2)
	fmt.Println("Sum is", sum)
}
