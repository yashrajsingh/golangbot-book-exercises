package main

import (
	"fmt"
	"unicode/utf8"
)

func printBytes(s string) {
	for i := 0; i < len(s); i++ {
		fmt.Printf("%x ", s[i])
	}
	fmt.Println()
}

func printChars(s string) {
	runes := []rune(s)
	for i := 0; i < len(runes); i++ {
		fmt.Printf("%c ", runes[i])
	}
}

func printCharsAndBytes(s string) {
	for index, rune := range s {
		fmt.Printf("%c starts at byte %d\n", rune, index)
	}
}

func length(s string) {
	fmt.Printf("length of %s is %d\n", s, utf8.RuneCountInString(s))
}

// func mutate(s string) string {
// 	s[0] = 'a' //any valid unicode character within single quote is a rune
// 	return s
// }

func mutate(s []rune) string {
	s[0] = 'a'
	return string(s)
}

func main() {
	name := "Hello World"
	fmt.Println(name)

	fmt.Println()

	printBytes(name)
	printChars(name)

	fmt.Println()

	name = "Señor"
	printBytes(name)
	fmt.Printf("\n")
	printChars(name)

	fmt.Println()

	printCharsAndBytes(name)

	fmt.Println()

	byteSlice := []byte{0x43, 0x61, 0x66, 0xC3, 0xA9}
	str := string(byteSlice)
	fmt.Println(str)

	fmt.Println()

	runeSlice := []rune{0x0053, 0x0065, 0x00f1, 0x006f, 0x0072}
	str = string(runeSlice)
	fmt.Println(str)

	fmt.Println()

	word2 := "Pets"
	length(word2)

	fmt.Println()

	// h := "hello"
	// fmt.Println(mutate(h))

	h := "hello"
	fmt.Println(mutate([]rune(h)))
}
