// Basic Types in GO:
// bool
// Numeric Types
//    int8, int16, int32, int64, int
//    uint8, uint16, uint32, uint64, uint
//    float32, float64
//    complex64, complex128
//    byte
//    rune
// string

package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var a bool = true
	b := false
	fmt.Println("a:", a, "b:", b)
	c := a && b
	fmt.Println("c:", c)
	d := a || b
	fmt.Println("d:", d)

	fmt.Println()

	// int8: represents 8 bit signed integers
	// size: 8 bits
	// range: -128 to 127
	//
	// int16: represents 16 bit signed integers
	// size: 16 bits
	// range: -32768 to 32767
	//
	// int32: represents 32 bit signed integers
	// size: 32 bits
	// range: -2147483648 to 2147483647
	//
	// int64: represents 64 bit signed integers
	// size: 64 bits
	// range: -9223372036854775808 to 9223372036854775807
	//
	// int: represents 32 or 64 bit integers depending on the underlying platform. You should generally be using int to represent integers unless there is a need to use a specific sized integer.
	// size: 32 bits in 32 bit systems and 64 bit in 64 bit systems.
	// range: -2147483648 to 2147483647 in 32 bit systems and -9223372036854775808 to 9223372036854775807 in 64 bit systems
	var e int = 89
	f := 95
	fmt.Println("\nvalue of e is", e, "and f is", f)
	fmt.Printf("\ntype of e is %T size of e is %d", e, unsafe.Sizeof(e)) //type and size of a
	fmt.Printf("\ntype of f is %T size of f is %d", f, unsafe.Sizeof(f)) //type and size of b

	fmt.Println()

	fmt.Println("\nvalue of a is", a, "and b is", b)
	fmt.Printf("\ntype of a is %T size of a is %d", a, unsafe.Sizeof(a)) //type and size of a
	fmt.Printf("\ntype of b is %T size of b is %d", b, unsafe.Sizeof(b)) //type and size of b

	fmt.Println()

	// uint8: represents 8 bit unsigned integers
	// size: 8 bits
	// range: 0 to 255
	//
	// uint16: represents 16 bit unsigned integers
	// size: 16 bits
	// range: 0 to 65535
	//
	// uint32: represents 32 bit unsigned integers
	// size: 32 bits
	// range: 0 to 4294967295
	//
	// uint64: represents 64 bit unsigned integers
	// size: 64 bits
	// range: 0 to 18446744073709551615
	//
	// uint : represents 32 or 64 bit unsigned integers depending on the underlying platform.
	// size : 32 bits in 32 bit systems and 64 bits in 64 bit systems.
	// range : 0 to 4294967295 in 32 bit systems and 0 to 18446744073709551615 in 64 bit systems
	//
	// Floating point types
	// float32: 32 bit floating point numbers
	// float64: 64 bit floating point numbers

	g, h := 5.67, 8.97
	fmt.Printf("\ntype of g %T h %T\n", g, h)

	fmt.Println()

	// complex64: complex numbers which have float32 real and imaginary parts
	// complex128: complex numbers with float64 real and imaginary parts
	i := 6 + 7i
	fmt.Println("\ncomplex no:", i)

	fmt.Println()

	c1 := complex(5, 7)
	c2 := 8 + 27i
	cadd := c1 + c2
	fmt.Println("sum:", cadd)
	cmul := c1 * c2
	fmt.Println("product:", cmul)

	fmt.Println()

	// byte is an alias of uint8
	// rune is an alias of int32

	// String is a collection of byte in golang
	first := "Naveen"
	last := "Ramanathan"
	name := first + " " + last
	fmt.Println("My name is", name)

	fmt.Println()

	// Type conversion is explicit in go
	k := 55           //int
	j := 67.8         //float64
	sum := k + int(j) //j is converted to int
	fmt.Println(sum)
}
