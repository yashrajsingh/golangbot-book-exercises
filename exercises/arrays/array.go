package main

import "fmt"

func changeLocal(num [5]int) {
	num[0] = 55
	fmt.Println("inside function ", num)
}

func printarray(a [3][2]string) {
	for _, v1 := range a {
		for _, v2 := range v1 {
			fmt.Printf("%s ", v2)
		}
		fmt.Printf("\n")
	}
}

func main() {
	var a [3]int //int array with length 3
	a[0] = 12    // array index starts at 0
	a[1] = 78
	a[2] = 50
	fmt.Println(a)

	fmt.Println()

	b := [3]int{12, 78, 50} // short hand declaration to create array
	fmt.Println(b)

	fmt.Println()

	c := [3]int{12}
	fmt.Println(c)

	fmt.Println()

	d := [...]int{12, 78, 50} // ... makes the compiler determine the length
	fmt.Println(d)

	fmt.Println()

	// e := [3]int{5, 78, 8}
	// var f [5]int
	// f = e //not possible since [3]int and [5]int are distinct types

	g := [...]string{"USA", "China", "India", "Germany", "France"}
	h := g // a copy of a is assigned to b
	h[0] = "Singapore"
	fmt.Println("g is ", g)
	fmt.Println("h is ", h)

	fmt.Println()

	num := [...]int{5, 6, 7, 8, 8}
	fmt.Println("before passing to function ", num)
	changeLocal(num) //num is passed by value
	fmt.Println("after passing to function ", num)

	fmt.Println()

	j := [...]float64{67.7, 89.8, 21, 78}
	for i := 0; i < len(j); i++ { //looping from 0 to the length of the array
		fmt.Printf("%d th element of a is %.2f\n", i, j[i])
	}

	fmt.Println()

	sum := float64(0)
	for i, v := range j { //range returns both the index and value
		fmt.Printf("%d the element of j is %.2f\n", i, v)
		sum += v
	}
	fmt.Println("\nsum of all elements of j", sum)

	fmt.Println()

	k := [3][2]string{
		{"lion", "tiger"},
		{"cat", "dog"},
		{"pigeon", "peacock"}, //this comma is necessary. The compiler will complain if you omit this comma
	}
	printarray(k)
	var l [3][2]string
	l[0][0] = "apple"
	l[0][1] = "samsung"
	l[1][0] = "microsoft"
	l[1][1] = "google"
	l[2][0] = "AT&T"
	l[2][1] = "T-Mobile"
	fmt.Printf("\n")
	printarray(l)
}
