package main

import "fmt"

// Employee is a named struct
type Employee struct {
	firstName, lastName string
	age, salary         int
}

type Person struct {
	string
	int
	Address
}

type Address struct {
	city, state string
}

type Spec struct { //exported struct
	Maker string //exported field
	model string //unexported field
	Price int    //exported field
}

type name struct {
	firstName string
	lastName  string
}

type image struct {
	data map[int]int
}

func main() {
	//creating structure using field names
	emp1 := Employee{
		firstName: "Sam",
		age:       25,
		salary:    500,
		lastName:  "Anderson",
	}
	//creating structure without using field names
	emp2 := Employee{"Thomas", "Paul", 29, 800}
	fmt.Println("Employee 1", emp1)
	fmt.Println("Employee 2", emp2)

	fmt.Println()

	// annonymous structures
	emp3 := struct {
		firstName, lastName string
		age, salary         int
	}{
		firstName: "Andreah",
		lastName:  "Nikola",
		age:       31,
		salary:    5000,
	}
	fmt.Println("Employee 3", emp3)

	fmt.Println()

	var emp4 Employee //zero valued structure
	fmt.Println("Employee 4", emp4)

	fmt.Println()

	emp5 := Employee{
		firstName: "John",
		lastName:  "Paul",
	} // some fields are zero valued
	fmt.Println("Employee 5", emp5)

	fmt.Println()

	emp8 := &Employee{"Sam", "Anderson", 55, 6000}
	// fmt.Println("First Name:", (*emp8).firstName)
	// fmt.Println("Age:", (*emp8).age)
	fmt.Println("First Name:", emp8.firstName)
	fmt.Println("Age:", emp8.age)

	fmt.Println()

	// annonymous fields struct & promoted fields
	var p1 Person
	p1.string = "naveen"
	p1.int = 50
	p1.Address = Address{
		city:  "Chicago",
		state: "Illinois",
	}
	fmt.Println(p1)
	fmt.Println("Name:", p1.string)
	fmt.Println("Age:", p1.int)
	fmt.Println("City:", p1.city)   //city is promoted field
	fmt.Println("State:", p1.state) //state is promoted field

	fmt.Println()

	// Structs are value types and are comparable if each of their fields are comparable. Two struct variables are considered equal if their corresponding non blank fields are equal.
	name1 := name{"Steve", "Jobs"}
	name2 := name{"Steve", "Jobs"}
	if name1 == name2 {
		fmt.Println("name1 and name2 are equal")
	} else {
		fmt.Println("name1 and name2 are not equal")
	}

	fmt.Println()

	name3 := name{firstName: "Steve", lastName: "Jobs"}
	name4 := name{}
	name4.firstName = "Steve"
	if name3 == name4 {
		fmt.Println("name3 and name4 are equal")
	} else {
		fmt.Println("name3 and name4 are not equal")
	}

	// Struct variables are not comparable if they contain fields which are not comparable
	// image1 := image{data: map[int]int{
	// 	0: 155,
	// }}
	// image2 := image{data: map[int]int{
	// 	0: 155,
	// }}
	// if image1 == image2 {
	// 	fmt.Println("image1 and image2 are equal")
	// }
}
