package main

import "fmt"

func main() {
	var age int // variable declaration
	fmt.Println("my age is ", age)

	fmt.Println()

	age = 54 //assignment
	fmt.Println("my new age is", age)

	fmt.Println()

	var age1 int = 29 // variable declaration with initial value
	fmt.Println("my age1 is", age1)

	fmt.Println()

	var age2 = 29 // type will be inferred
	fmt.Println("my age2 is", age2)

	fmt.Println()

	var width, height = 100, 50 //declaring multiple variables
	fmt.Println("width is", width, "height is", height)

	fmt.Println()

	var (
		firstname string
		surname   string
	)
	fmt.Println("name:", firstname, surname)
	firstname, surname = "yash", "raj singh"
	fmt.Println("name:", firstname, surname)

	fmt.Println()

	name, age := "naveen", 29 //short hand declaration
	fmt.Println("my name is", name, "age is", age)

	fmt.Println()

	a, b := 20, 30 // declare variables a and b
	fmt.Println("a is", a, "b is", b)
	b, c := 40, 50 // b is already declared but c is new
	fmt.Println("b is", b, "c is", c)
	b, c = 80, 90 // assign new values to already declared variables b and c
	fmt.Println("changed b is", b, "c is", c)
}
