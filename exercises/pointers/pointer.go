package main

import (
	"fmt"
)

func change(val *int) {
	*val = 55
}

func modify(arr []int) {
	// func modify(arr *[3]int) {
	// (*arr)[0] = 90
	arr[0] = 90 // shorthand
}

func main() {
	b := 255
	var a *int = &b
	fmt.Printf("Type of a is %T\n", a)
	fmt.Println("address of b is", a)

	fmt.Println()

	c := 25
	var d *int
	if d == nil {
		fmt.Println("d is", d)
		d = &c
		fmt.Println("d after initialization is", d)
	}

	fmt.Println()

	e := 255
	// Dereferencing e
	f := &e
	fmt.Println("address of e is", f)
	fmt.Println("value of e is", *f)
	*f++
	fmt.Println("new value of e is", e)

	fmt.Println()

	change(f)
	fmt.Println("value of a after function call is", e)

	fmt.Println()

	g := [3]int{89, 90, 91}
	// modify(&g)
	modify(g[:])
	fmt.Println(g)
}
