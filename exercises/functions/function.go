package main

import "fmt"

func calculateBill(price int, no int) int {
	var totalPrice = price * no
	return totalPrice
}

func rectProps(length, width float64) (area, perimeter float64) {
	area = length * width
	perimeter = (length + width) * 2
	return //no explicit return value
}

func main() {
	fmt.Println("bill:", calculateBill(12, 2))

	fmt.Println()

	area, perimeter := rectProps(10.8, 5.6)
	fmt.Printf("Area %f Perimeter %f\n", area, perimeter)
}
